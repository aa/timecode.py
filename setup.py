from distutils.core import setup

setup(
    name='timecode',
    version='0.1.0',
    author='Active Archives Contributors',
    py_modules=['timecode']
)
