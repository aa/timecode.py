Basic timecode functionality.


Examples:

```python
import timecode

t = 66.0
tc = timecode.timecode_fromsecs(t, fract=True, alwaysfract=False)
print tc

t = 66.5
tc = timecode.timecode_fromsecs(t, fract=True, alwaysfract=False)
print tc

tc = timecode.timecode_fromsecs(t, fract=False)
print tc

```

produces:

	01:06

	01:06,500

	01:07


Installing
------------

	python setup.py install
